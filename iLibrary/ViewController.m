//
//  ViewController.m
//  iLibrary
//
//  Created by Soumya Mandi on 8/23/13.
//  Copyright (c) 2013 Soumya Mandi. All rights reserved.
//

#import "ViewController.h"
#define NUM_BOOKS_IN_SHELF 3
@interface ViewController ()
{
    NSArray *booksData;
}

@end

@implementation ViewController


- (CGPDFDocumentRef)getPDFFromFileName:(NSString *)fileName
{
    
    //NSLog(@"VC.getPDFFromFile***");
    NSString *pdfPath = [[NSBundle mainBundle]
                         pathForResource:fileName ofType:@"pdf"];
    NSURL *pdfUrl = [NSURL fileURLWithPath:pdfPath];
    CGPDFDocumentRef pdfRef = CGPDFDocumentCreateWithURL((__bridge CFURLRef)pdfUrl);
    return pdfRef;
    
}

- (UIImage *)imageFromPDFWithDocumentRef:(CGPDFDocumentRef)documentRef {
    CGPDFPageRef pageRef = CGPDFDocumentGetPage(documentRef, 1);
    size_t numpages  = CGPDFDocumentGetNumberOfPages(documentRef);
    //NSLog(@"num pages is %zu", numpages);
    CGRect pageRect = CGPDFPageGetBoxRect(pageRef, kCGPDFCropBox);
    
    UIGraphicsBeginImageContext(pageRect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, CGRectGetMinX(pageRect),CGRectGetMaxY(pageRect));
    CGContextScaleCTM(context, 1, -1);
    CGContextTranslateCTM(context, -(pageRect.origin.x), -(pageRect.origin.y));
    CGContextDrawPDFPage(context, pageRef);
    
    UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return finalImage;
}




- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    //booksData = [NSArray arrayWithObjects:@"city", @"guitar", @"face",@"city", @"kanctydig", @"face",  nil];
    
    booksData = [NSArray arrayWithObjects:@"city", @"guitar", @"face",@"TestPage",@"svd",@"city", @"guitar", nil];
    //NSLog(@"after viewDidLaod");

}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //NSLog(@"numRows is : %d", ([booksData count]+2)/3);
    return ([booksData count]+2)/3;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"tableView cellForRowAtIndexPath");
    static NSString *cellIdentifier = @"BOOKRACK";
    
    GroupedCell *cell = (GroupedCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier ];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:nil options:nil];
        cell = [nib objectAtIndex:0];
    }
    //NSLog(@"before initializing docs");
    CGPDFDocumentRef pdfDocFirst  = [self getPDFFromFileName:[booksData objectAtIndex:(indexPath.row*3)] ];
    [cell.firstButton setImage:[self imageFromPDFWithDocumentRef:pdfDocFirst] forState:nil];

    //NSLog(@"numBooks is : %d", [booksData count]);
    //NSLog(@"index is : %d", indexPath.row);
    if ([booksData count] == indexPath.row*NUM_BOOKS_IN_SHELF+1) {
        [cell.secondButton removeFromSuperview];
        [cell.thirdButton removeFromSuperview];
        return cell;
    }
    
    CGPDFDocumentRef pdfDocSecond = [self getPDFFromFileName:[booksData objectAtIndex:(indexPath.row*3+1)] ];
    [cell.secondButton setImage:[self imageFromPDFWithDocumentRef:pdfDocSecond] forState:nil];
    if ([booksData count] == indexPath.row*NUM_BOOKS_IN_SHELF+2) {
        [cell.thirdButton removeFromSuperview];
        return cell;
    }
    
    
    
    CGPDFDocumentRef pdfDocThird  = [self getPDFFromFileName:[booksData objectAtIndex:(indexPath.row*3+2)] ];
    [cell.thirdButton setImage:[self imageFromPDFWithDocumentRef:pdfDocThird] forState:nil];
    
    
    return cell;
}

- (CGPDFDocumentRef)getPDFDocumentFromSelectedRow:(NSInteger )rowIndex andShelfIndex:(NSInteger)bookIndex
{
    NSInteger bookIndexInLibrary = NUM_BOOKS_IN_SHELF*rowIndex+bookIndex;
    //NSLog(@"book at index is : %@", [booksData objectAtIndex:bookIndexInLibrary]);
    NSString *bookName = [booksData objectAtIndex:bookIndexInLibrary];
    CGPDFDocumentRef requestedPDFDocument = [self getPDFFromFileName:bookName];
    
    return requestedPDFDocument;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    //NSLog(@"hello seague");
    
    if ([[segue identifier] isEqualToString:@"FirstBookSegue"]) {
        UIButton *button = (UIButton*)sender;
        UITableViewCell* cell = (UITableView *)[[button superview ]superview];
        
        NSIndexPath *selectedIndexPath = [self.tableView indexPathForCell:cell];
        //NSLog(@"row selected is : %d", selectedIndexPath.row);
        NSInteger row = selectedIndexPath.row;
        
        CGPDFDocumentRef PDFToOpen = [self getPDFDocumentFromSelectedRow:row andShelfIndex:0];
        
        if (segue.destinationViewController != nil) {
            
            DetailViewController *detailViewController = (DetailViewController *)segue.destinationViewController;
            
            [detailViewController setPdfName:[booksData objectAtIndex:(NUM_BOOKS_IN_SHELF*row)]];
            //NSLog(@"name of books is %@", [booksData objectAtIndex:(NUM_BOOKS_IN_SHELF*row)]);
            
            //Animations
            
            UIView *oldView = ((UIViewController *)segue.sourceViewController).view;
            UIView *newView = ((UIViewController *)segue.destinationViewController).view;
            
            [oldView.window insertSubview:newView aboveSubview:oldView];
            float offsetY = newView.center.y  - oldView.center.y ;
            //NSLog(@"old y = %f, new y = %f , offsetY = %f", oldView.center.y, newView.center.y, offsetY);
            newView.center = CGPointMake(oldView.center.x + oldView.frame.size.width, oldView.center.y + offsetY);
            
            [UIView animateWithDuration:0.3
                                  delay:0.3
                                options:UIViewAnimationOptionTransitionCurlUp
             //options:UIViewAnimationCurveEaseOut
                             animations:^{ newView.center = CGPointMake(oldView.center.x, oldView.center.y + offsetY);}
                             completion:^(BOOL finished){ [oldView removeFromSuperview]; }];
 
        }
    }
    else if ([[segue identifier] isEqualToString:@"SecondBookSegue"]) {
        //NSLog(@"Second Segue running");
        //NSLog(@"sender is : %@", sender);
        UIButton *button = (UIButton*)sender;
        //NSLog(@"superView is : %@",button.superview);
        UITableViewCell* cell = (UITableView *)[[button superview ]superview];
        
        
        NSIndexPath *selectedIndexPath = [self.tableView indexPathForCell:cell];
        //NSLog(@"row selected is : %d", selectedIndexPath.row);
        NSInteger row = selectedIndexPath.row;
                
        if (segue.destinationViewController != nil) {
            
            DetailViewController *detailViewController = (DetailViewController *)segue.destinationViewController;
            
            [detailViewController setPdfName:[booksData objectAtIndex:(NUM_BOOKS_IN_SHELF*row+1)]];
            //NSLog(@"name of books is %@", [booksData objectAtIndex:(NUM_BOOKS_IN_SHELF*row+1)]);
            
            //[myViewController setBookString:booksData[NUM_BOOKS_IN_SHELF*row+1]];
            //NSLog(@"book selected is %@",[booksData objectAtIndex:(NUM_BOOKS_IN_SHELF*row+1)]);

            //NSLog(@"After getting done with prepareForSegue");
        }
        
        
        //Animation
        
        UIView *oldView = ((UIViewController *)segue.sourceViewController).view;
        UIView *newView = ((UIViewController *)segue.destinationViewController).view;
        
        [oldView.window insertSubview:newView aboveSubview:oldView];
        float offsetY = newView.center.y  - oldView.center.y ;
        //NSLog(@"old y = %f, new y = %f , offsetY = %f", oldView.center.y, newView.center.y, offsetY);
        newView.center = CGPointMake(oldView.center.x + oldView.frame.size.width, oldView.center.y + offsetY);
        
        [UIView animateWithDuration:0.3
                              delay:0.3
                            options:UIViewAnimationCurveLinear
         //options:UIViewAnimationCurveEaseOut
                         animations:^{ newView.center = CGPointMake(oldView.center.x, oldView.center.y + offsetY);}
                         completion:^(BOOL finished){ [oldView removeFromSuperview]; }];
        
    }
    else if ([[segue identifier] isEqualToString:@"ThirdBookSegue"]) {
        //NSLog(@"Third Segue running");
        UIButton *button = (UIButton*)sender;
        //NSLog(@"superView is : %@",button.superview);
        UITableViewCell* cell = (UITableView *)[[button superview ]superview];
        
        
        NSIndexPath *selectedIndexPath = [self.tableView indexPathForCell:cell];
        //NSLog(@"row selected is : %d", selectedIndexPath.row);
        NSInteger row = selectedIndexPath.row;
        
        if (segue.destinationViewController != nil) {
            //NSLog(@"segue.destVC is not nil");
            
            DetailViewController *detailViewController = (DetailViewController *)segue.destinationViewController;
            
            [detailViewController setPdfName:[booksData objectAtIndex:(NUM_BOOKS_IN_SHELF*row+2)]];
            //NSLog(@"name of books is %@", [booksData objectAtIndex:(NUM_BOOKS_IN_SHELF*row+2)]);
            
            
        }
        //Animation
        
        UIView *oldView = ((UIViewController *)segue.sourceViewController).view;
        UIView *newView = ((UIViewController *)segue.destinationViewController).view;
        
        [oldView.window insertSubview:newView aboveSubview:oldView];
        float offsetY = newView.center.y  - oldView.center.y ;
        //NSLog(@"old y = %f, new y = %f , offsetY = %f", oldView.center.y, newView.center.y, offsetY);
        newView.center = CGPointMake(oldView.center.x + oldView.frame.size.width, oldView.center.y + offsetY);
        
        [UIView animateWithDuration:0.3
                              delay:0.3
                            options:UIViewAnimationCurveLinear
         //options:UIViewAnimationCurveEaseOut
                         animations:^{ newView.center = CGPointMake(oldView.center.x, oldView.center.y + offsetY);}
                         completion:^(BOOL finished){ [oldView removeFromSuperview]; }];
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
