//
//  ViewController.h
//  iLibrary
//
//  Created by Soumya Mandi on 8/23/13.
//  Copyright (c) 2013 Soumya Mandi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupedCell.h"
#import "DetailViewController.h"
@interface ViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
