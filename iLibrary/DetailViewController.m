//
//  DetailViewController.m
//  iLibrary
//
//  Created by Soumya Mandi on 8/23/13.
//  Copyright (c) 2013 Soumya Mandi. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController
@synthesize pdfName,pdfDoc,numberOfPages;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


/* appends .pdf to nameOfBook and returns a CGPDFDocumentRef */
-(void)setPDFDocumentFromName
{
    NSLog(@"SPDFV.getPDFDFN nameOfBook is : %@", pdfName);
    NSString *fullName = [pdfName stringByAppendingString:@".pdf"];
    NSLog(@"fullName is : %@", fullName);
    CFURLRef pdfURL = CFBundleCopyResourceURL(CFBundleGetMainBundle(), (CFStringRef)CFBridgingRetain(fullName), NULL, NULL);
    pdfDoc = CGPDFDocumentCreateWithURL((CFURLRef)pdfURL);
}
-(CGPDFDocumentRef)pdfDocument
{
    NSLog(@"PVC.pdfDocument()");
	if (pdfDoc == NULL)
	{
		CFURLRef pdfURL = CFBundleCopyResourceURL(CFBundleGetMainBundle(), CFSTR("guitar.pdf"), NULL, NULL);
		pdfDoc = CGPDFDocumentCreateWithURL((CFURLRef)pdfURL);
		//CFRelease(pdfURL);
	}   
    return pdfDoc;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSLog(@"pdfName is %@", pdfName);
    [self setPDFDocumentFromName];
    
    size_t numPages = CGPDFDocumentGetNumberOfPages(pdfDoc);
    numberOfPages = numPages;
    NSLog(@"PVC.vDL() numberOfPages is %d", numberOfPages);
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStylePageCurl navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    self.pageController.dataSource = self;
    [[self.pageController view] setFrame:[[self view] bounds]];
    
    ChildViewController *initialViewController = [self viewControllerAtIndex:0];
    
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [self addChildViewController:self.pageController];
    [[self view] addSubview:[self.pageController view]];

}




- (ChildViewController *)viewControllerAtIndex:(NSUInteger)index
{
    NSLog(@"PVC.vCAI() ");
    if (pdfDoc == nil) {
        NSLog(@"pdfDoc is nil ;(");
    }
    else {
        NSLog(@"pdfDoc is good :)");
    }
    ChildViewController *childViewController = [[ChildViewController alloc] initWithNibName:@"ChildViewController" bundle:nil];
    childViewController.index = index;
    
    CGPDFPageRef page = CGPDFDocumentGetPage(pdfDoc, index+1);
    if (page == nil) {
        NSLog(@"pdf's page is nil :(");
    }
    else {
        NSLog(@"pdf's page is good :)");
    }
    
    //set the pdf page
    [childViewController setPdfPage:page];
    
    return childViewController;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSLog(@"PVC.pageVC vCBVC");
    NSUInteger index = [(ChildViewController *)viewController index];
    
    if (index == 0) {
        return nil;
    }
    
    // Decrease the index by 1 to return
    index--;
    
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSLog(@"PVC.pageVC vCAVC");
    
    NSUInteger index = [(ChildViewController *)viewController index];
    
    index++;
    
    if (index == numberOfPages-1) {
        return nil;
        
    }
    
    return [self viewControllerAtIndex:index];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
