//
//  GroupedCell.h
//  iBook
//
//  Created by Soumya Mandi on 8/14/13.
//  Copyright (c) 2013 Soumya Mandi. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface GroupedCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *firstButton;
@property (weak, nonatomic) IBOutlet UIButton *secondButton;
@property (weak, nonatomic) IBOutlet UIButton *thirdButton;

@end
