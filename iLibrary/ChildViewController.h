//
//  ChildViewController.h
//  ZoomPDF
//
//  Created by Soumya Mandi on 8/23/13.
//  Copyright (c) 2013 Soumya Mandi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDFScrollView.h"
@interface ChildViewController : UIViewController
@property (weak, nonatomic) IBOutlet PDFScrollView *scrollView;

@property (assign, nonatomic) NSInteger index;
@property (assign, nonatomic) CGPDFPageRef pdfPage;
@end
