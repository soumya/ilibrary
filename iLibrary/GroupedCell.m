//
//  GroupedCell.m
//  iBook
//
//  Created by Soumya Mandi on 8/14/13.
//  Copyright (c) 2013 Soumya Mandi. All rights reserved.
//

#import "GroupedCell.h"

@implementation GroupedCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
