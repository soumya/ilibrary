//
//  ChildViewController.m
//  ZoomPDF
//
//  Created by Soumya Mandi on 8/23/13.
//  Copyright (c) 2013 Soumya Mandi. All rights reserved.
//

#import "ChildViewController.h"

@interface ChildViewController () {
    UIDeviceOrientation orientation;
}
    
@end

@implementation ChildViewController
@synthesize pdfPage;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    CGSize cvcViewSize = self.view.frame.size;
    NSLog(@"CVC.vDL() and cvcViewSize is %@", [NSValue valueWithCGSize:cvcViewSize]);
    NSLog(@"CVCV.vDL() and scrollViewSize is %@", [NSValue valueWithCGSize:self.scrollView.frame.size]);
    /*
    if (self.scrollView == nil) {
        NSLog(@"CVC.vDL() self.scrollView is nil");
    }
    else {
        NSLog(@"CVC.vDL() self.scrollView is not nil");
    }
     */
    //[self.view setBackgroundColor:[UIColor redColor]];
    //[self.scrollView setBackgroundColor:[UIColor blueColor]];
    
	// Make the view aware of orientation changes.
    /*
	    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
	    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRotate:) name:@"UIDeviceOrientationDidChangeNotification"  object:nil];
	    orientation = [[UIDevice currentDevice] orientation];
	    if (orientation == UIDeviceOrientationUnknown || orientation == UIDeviceOrientationFaceUp || orientation == UIDeviceOrientationFaceDown) {
	        orientation = UIDeviceOrientationPortrait;
	    }
    */
    
    
    [self.scrollView setPDFPage:pdfPage];
    CGRect pageRect = CGPDFPageGetBoxRect(pdfPage, kCGPDFArtBox);
    
    if (orientation == UIDeviceOrientationLandscapeLeft) {
        self.scrollView.contentSize = pageRect.size;
        //self.scrollView.contentSize = CGRectMake(0, 0, <#CGFloat height#>)    CGPDFPageGetBoxRect(pdfPage, kCGPDFMediaBox);
    }
    self.scrollView.contentSize = pageRect.size;
    [self.view addSubview:self.scrollView];
}

	// orientation view swapping logic
- (void)didRotate:(NSNotification *)notification
{
        UIDeviceOrientation newOrientation = [[UIDevice currentDevice] orientation];
        if (newOrientation != UIDeviceOrientationUnknown && newOrientation != UIDeviceOrientationFaceUp && newOrientation != UIDeviceOrientationFaceDown) {
                orientation = newOrientation;
        }
    
    //Orientation logic goes here //    CGRect pageRect = CGPDFPageGetBoxRect(_PDFPage, kCGPDFMediaBox);

    if (orientation == UIDeviceOrientationLandscapeLeft) {
        CGRect pageRect = CGPDFPageGetBoxRect(pdfPage, kCGPDFMediaBox);
        pageRect.size.height *= 1;

        self.scrollView.contentSize = pageRect.size;
        
        //self.scrollView.contentSize = CGRectMake(0, 0, <#CGFloat height#>)    CGPDFPageGetBoxRect(pdfPage, kCGPDFMediaBox);
    }
    else if (orientation == UIDeviceOrientationPortrait) {
        
    }
    else if (orientation == UIDeviceOrientationPortrait) {
        
    }
    else if (orientation == UIDeviceOrientationLandscapeRight) {
        CGRect pageRect = CGPDFPageGetBoxRect(pdfPage, kCGPDFMediaBox);
        self.scrollView.contentSize = pageRect.size;
        //[self.scrollView setPDFPage:pdfPage];
            }
    	// Do your orientation logic here
    /*
        if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
        	        // Clear the current view and insert the orientation specific view.
            [self clearCurrentView];
            [self.view insertSubview:mainLandscapeView atIndex:0];
        } else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
        // Clear the current view and insert the orientation specific view.
        [self clearCurrentView];
        [self.view insertSubview:mainPortraitView atIndex:0];
            	    }
    
     */
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
