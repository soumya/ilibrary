//
//  DetailViewController.h
//  iLibrary
//
//  Created by Soumya Mandi on 8/23/13.
//  Copyright (c) 2013 Soumya Mandi. All rights reserved.
//




#import <UIKit/UIKit.h>
#import "PDFScrollView.h"
#import "ChildViewController.h"
@interface DetailViewController : UIViewController <UIPageViewControllerDataSource>{
    NSString *pdfName;
}
@property (strong, nonatomic) UIPageViewController *pageController;

@property (strong, nonatomic) NSString *pdfName;
@property (strong, nonatomic) NSMutableArray *pdfPageArray;
@property (assign, nonatomic) CGPDFDocumentRef pdfDoc;
@property (assign,nonatomic) NSInteger numberOfPages;
@end
