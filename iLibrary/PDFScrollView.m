//
//  PDFScrollView.m
//  ZoomPDF
//
//  Created by Soumya Mandi on 8/23/13.
//  Copyright (c) 2013 Soumya Mandi. All rights reserved.
//

#import "PDFScrollView.h"
#import <QuartzCore/QuartzCore.h>


@interface PDFScrollView ()

// A low resolution image of the PDF page that is displayed until the TiledPDFView renders its content.
@property (nonatomic, weak) UIImageView *backgroundImageView;

// The TiledPDFView that is currently front most.
@property (nonatomic, weak) TiledPDFView *tiledPDFView;

// The old TiledPDFView that we draw on top of when the zoom stops
@property (nonatomic, weak) TiledPDFView *oldTiledPDFView;

@end
@implementation PDFScrollView
{
    CGPDFPageRef _PDFPage;
    
    //Current PDF zoom scale.
    CGFloat _PDFScale;
    
    CGFloat _PDFScaleX;
    CGFloat _PDFScaleY;
}
@synthesize backgroundImageView, tiledPDFView, oldTiledPDFView;

/*
- (id)initWithFrame:(CGRect)frame
{
    NSLog(@"PDFScV.initWithFrame()");
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.decelerationRate = UIScrollViewDecelerationRateFast;
        self.delegate = self;
        self.minimumZoomScale = 0.5;
        self.maximumZoomScale = 2.0;
        [self setZoomScale:self.minimumZoomScale];
        
    }
    return self;
}
*/


- (id)initWithCoder:(NSCoder *)coder
{
    NSLog(@"PDFScV.initWithCoder()");
    self = [super initWithCoder:coder];
    if (self) {
        self.decelerationRate = UIScrollViewDecelerationRateFast;
        self.delegate = self;
        self.minimumZoomScale = 0.5;
        self.maximumZoomScale = 4.0;
        [self setZoomScale:0.8];

    }
    return self;
}


- (void)setPDFPage:(CGPDFPageRef)PDFPage;
{
    //NSLog(@"PDFScV.setPDFPage()");
    //NSLog(@"PDFScV.setPDFPage page index is : %d",CGPDFPageGetPageNumber(PDFPage));
    CGPDFPageRetain(PDFPage);
    CGPDFPageRelease(_PDFPage);
    _PDFPage = PDFPage;
    // Determine the size of the PDF page.
    CGRect pageRect = CGPDFPageGetBoxRect(_PDFPage, kCGPDFMediaBox);
    NSLog(@"PDFScVi.setPDFPage() pageRect size for PDF  is %@", [NSValue valueWithCGRect:pageRect]);
    NSLog(@"PDFScVi.setPDFPage() scroll size is %@", [NSValue valueWithCGRect:self.frame]);
    //_PDFScale = self.frame.size.width/pageRect.size.width;(*)
    _PDFScale = self.frame.size.width/pageRect.size.width;
    //_PDFScale = 1.30;
    
    //pageRect.size = CGSizeMake(768, 1024);
    //pageRect.size = CGSizeMake(pageRect.size.width*_PDFScale, pageRect.size.height*_PDFScale);//(*)
    pageRect.size = CGSizeMake(self.frame.size.width, self.frame.size.height);
    
    
    /*
     Create a low resolution image representation of the PDF page to display before the TiledPDFView renders its content.
     */
    /*
    UIGraphicsBeginImageContext(pageRect.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // First fill the background with white.
    CGContextSetRGBFillColor(context, 1.0,1.0,1.0,1.0);
    CGContextFillRect(context,pageRect);
    
    CGContextSaveGState(context);
    // Flip the context so that the PDF page is rendered right side up.
    CGContextTranslateCTM(context, 0.0, pageRect.size.height);
    CGContextScaleCTM(context, 1.5, -1.0);
    
    // Scale the context so that the PDF page is rendered at the correct size for the zoom level.
    CGContextScaleCTM(context, _PDFScale,_PDFScale);
    //CGContextDrawPDFPage(context, _PDFPage);
    CGContextRestoreGState(context);
    
    NSLog(@"after drawing the _PDFPage");
    UIImage *backgroundImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    
    if (self.backgroundImageView != nil) {
        [self.backgroundImageView removeFromSuperview];
    }
    
     UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
     backgroundImageView.frame = pageRect;
     backgroundImageView.contentMode = UIViewContentModeScaleAspectFit;
     [self addSubview:backgroundImageView];
     ;
    [self sendSubviewToBack:backgroundImageView];
     self.backgroundImageView = backgroundImageView;
     *///uncomment for original code
    // Create the TiledPDFView based on the size of the PDF page and scale it to fit the view.
    NSLog(@"PDFScVi.setPDFPage() pageRect is %@", [NSValue valueWithCGRect:pageRect]);
    TiledPDFView *tiledPDFView = [[TiledPDFView alloc] initWithFrame:pageRect scale:_PDFScale];// (*)
    //TiledPDFView *tiledPDFView = [[TiledPDFView alloc] initWithFrame:pageRect];
    [tiledPDFView setPage:_PDFPage];
    self.userInteractionEnabled = YES;
    
    NSLog(@"pageRect is %@", [NSValue valueWithCGRect:pageRect]);
    NSLog(@"self.frame is %@", [NSValue valueWithCGRect:self.frame]);
    //[self setContentSize:self.frame.size];
    
    [self addSubview:tiledPDFView];
    self.tiledPDFView = tiledPDFView;
    
}

#pragma mark -
#pragma mark Override layoutSubviews to center content

// Use layoutSubviews to /Users/soumya/iOS/iLibrary/iLibrary.xcodeprojcenter the PDF page in the view.

- (void)layoutSubviews
{
    [super layoutSubviews];
    //NSLog(@"PDFScrollView.layoutSubviews");
    // Center the image as it becomes smaller than the size of the screen.
    NSLog(@"PDFScVi.layoutSubViews scroll.frame is %@", [NSValue valueWithCGRect:self.frame]);
    //[self setBackgroundColor:[UIColor blueColor]];
    CGSize boundsSize = self.bounds.size;
    CGRect frameToCenter = self.tiledPDFView.frame;
    //NSLog(@"PDFScV.boundsSize is : %@", [NSValue valueWithCGSize:boundsSize]);
    // Center horizontally.
    
    if (frameToCenter.size.width < boundsSize.width)
        frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
    else
        frameToCenter.origin.x = 0;
    
    // Center vertically.
    
    if (frameToCenter.size.height < boundsSize.height)
        frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
    else
        frameToCenter.origin.y = 0;
    
    self.tiledPDFView.frame = frameToCenter;
    //self.backgroundImageView.frame = frameToCenter;
    
    /*
     To handle the interaction between CATiledLayer and high resolution screens, set the tiling view's contentScaleFactor to 1.0.
     If this step were omitted, the content scale factor would be 2.0 on high resolution screens, which would cause the CATiledLayer to ask for tiles of the wrong scale.
     */
    self.tiledPDFView.contentScaleFactor = 1.0;
}


#pragma mark -
#pragma mark UIScrollView delegate methods

/*
 A UIScrollView delegate callback, called when the user starts zooming.
 Return the current TiledPDFView.
 */
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.tiledPDFView;
}

/*
 A UIScrollView delegate callback, called when the user begins zooming.
 When the user begins zooming, remove the old TiledPDFView and set the current TiledPDFView to be the old view so we can create a new TiledPDFView when the zooming ends.
 */
/*
- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view
{
    // Remove back tiled view.
    [self.oldTiledPDFView removeFromSuperview];
    
    // Set the current TiledPDFView to be the old view.
    self.oldTiledPDFView = self.tiledPDFView;
    [self addSubview:self.oldTiledPDFView];
}
*/

/*
 A UIScrollView delegate callback, called when the user stops zooming.
 When the user stops zooming, create a new TiledPDFView based on the new zoom level and draw it on top of the old TiledPDFView.
 */
/*
- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale
{
    // Set the new scale factor for the TiledPDFView.
    _PDFScale *= scale;
    
    // Calculate the new frame for the new TiledPDFView.
    CGRect pageRect = CGPDFPageGetBoxRect(_PDFPage, kCGPDFMediaBox);
    pageRect.size = CGSizeMake(pageRect.size.width*_PDFScale, pageRect.size.height*_PDFScale);
    
    // Create a new TiledPDFView based on new frame and scaling.
    TiledPDFView *tiledPDFView = [[TiledPDFView alloc] initWithFrame:pageRect scale:_PDFScale];
    [tiledPDFView setPage:_PDFPage];
    
    // Add the new TiledPDFView to the PDFScrollView.
    [self addSubview:tiledPDFView];
    self.tiledPDFView = tiledPDFView;
}

*/

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
